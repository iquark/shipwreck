package com.sapienstree.naufragos.assets;

public class Wave {
    private float x, y;
    
    public Wave() {
    	this.x = 0;
    	this.y = 0;
    }

    public Wave(float x, float y) {
    	this.x = x;
    	this.y = y;
    }
    
    public float getX() {
        return x;
    }
    
    public float getY() {
        return y;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }
}
