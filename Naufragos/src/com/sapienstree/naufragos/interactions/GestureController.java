package com.sapienstree.naufragos.interactions;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class GestureController implements GestureListener {
	float velX, velY;
	boolean flinging = false;
	float initialScale = 1;
	final Integer MAX_VELOCITY = 5000;
	Rectangle item;
	
	public GestureController(Rectangle item) {
		this.item = item;
	}
	
	public boolean touchDown (float x, float y, int pointer, int button) {
		flinging = false;
		return false;
	}

	@Override
	public boolean tap (float x, float y, int count, int button) {
		Gdx.app.log("GestureDetectorTest", "tap at " + x + ", " + y + ", count: " + count);
		this.item.x = x-128/2;
		this.item.y = Gdx.graphics.getHeight()-y-128/2;
		return false;
	}

	@Override
	public boolean longPress (float x, float y) {
		Gdx.app.log("GestureDetectorTest", "long press at " + x + ", " + y);
		this.item.x = x-128/2;
		this.item.y = Gdx.graphics.getHeight()-y-128/2;
		return false;
	}

	@Override
	public boolean fling (float velocityX, float velocityY, int button) {
		flinging = true;
		velX = (velocityX<=MAX_VELOCITY)?velocityX:MAX_VELOCITY;
		velY = (Math.abs(velocityY)<=MAX_VELOCITY)?Math.abs(velocityY):MAX_VELOCITY;
		return false;
	}

	@Override
	public boolean pan (float x, float y, float deltaX, float deltaY) {
		return false;
	}

	@Override
	public boolean zoom (float originalDistance, float currentDistance) {
		return false;
	}

	@Override
	public boolean pinch (Vector2 initialFirstPointer, Vector2 initialSecondPointer, Vector2 firstPointer, Vector2 secondPointer) {
		return false;
	}

	public void update() {
		if (flinging) {
			this.item.x += velX * Gdx.graphics.getDeltaTime();
			this.item.y += velY * Gdx.graphics.getDeltaTime();
			if (Math.abs(velX) < 0.01f) velX = 0;
			if (Math.abs(velY) < 0.01f) velY = 0;
		}
	}
}

