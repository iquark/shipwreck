package com.sapienstree.naufragos;

import java.util.ArrayList;
import java.util.Iterator;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenManager;
import aurelienribon.tweenengine.equations.Bounce;
import aurelienribon.tweenengine.equations.Linear;
import aurelienribon.tweenengine.equations.Sine;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Rectangle;
import com.sapienstree.naufragos.assets.Wave;
import com.sapienstree.naufragos.interactions.GestureController;
import com.sapienstree.naufragos.tweens.SpriteAccessor;
import com.sapienstree.naufragos.tweens.WaveAccessor;

public class Naufragos implements ApplicationListener {
	SpriteBatch batch;
	OrthographicCamera camera;
	BitmapFont font;
	BitmapFont font2;
	Integer width;
	Integer height;
	// interactions
	String flingresult;
	GestureController controller;
	GestureDetector gestureDetector;
	// Tween
	TweenManager manager;
	// backgrounds
	Texture texture;
	// live saver
	Rectangle lifeSaver;
	Texture lifeSaverTex;
	// waves
	ArrayList<Sprite> waveSpr;
	ArrayList<Texture> waveTex;
	Wave wave;
	int waveHeight=128;
	// men
	ArrayList<Texture> manTex;
	Sprite manSpr;
	int manWidth = 110, manHeight = 128;
	
	@Override
	public void create() {
		height = Gdx.graphics.getHeight();
		width = Gdx.graphics.getWidth();
		flingresult = "";

		texture = new Texture(Gdx.files.internal("data/stones.jpg"));
		
		// live Saver
		lifeSaverTex = new Texture(Gdx.files.internal("data/LifeSaver.png"));
		
		// liveSaver mask
		lifeSaver = new Rectangle();
		lifeSaver.x = 800/2 - 90;
		lifeSaver.y = 10; 
		lifeSaver.width = 90;
		lifeSaver.height = 90;
				
		// Man
		manTex = new ArrayList<Texture>(5);
		manTex.add(new Texture(Gdx.files.internal("data/man1.png")));
		manTex.add(new Texture(Gdx.files.internal("data/man2.png")));
		manSpr = new Sprite(manTex.get(0));
		manSpr.setPosition(200, 400);
		manSpr.setSize(manWidth, manHeight);
		
		// Waves
		waveTex = new ArrayList<Texture>(5);
		waveTex.add(new Texture(Gdx.files.internal("data/wave1.png")));
		waveTex.add(new Texture(Gdx.files.internal("data/wave2.png")));
		waveTex.add(new Texture(Gdx.files.internal("data/wave3.png")));
		waveTex.add(new Texture(Gdx.files.internal("data/wave4.png")));
		waveTex.add(new Texture(Gdx.files.internal("data/wave5.png")));
		
		waveSpr = new ArrayList<Sprite>(waveTex.size()*2);
		
		Iterator<Sprite> iterator = waveSpr.iterator();
		boolean odd = true;
		int position = 500;

        Tween.registerAccessor(Sprite.class, new SpriteAccessor());
        
		for (int idx=0, wdx=0; idx<waveTex.size()*2; idx++) {
			Sprite spr;
			
			if (idx%2!=0) {
				spr = new Sprite(waveTex.get(wdx));
				spr.setPosition(width/2-waveTex.get(wdx++).getWidth()/2, position);
			} else {
				spr = new Sprite(manTex.get((int)(Math.round(Math.random()/1))));
				spr.setPosition(((float)(Math.random()))*(width-manWidth), position);
				position -= waveHeight - 50;
			}	
	        waveSpr.add(spr);
		}

		manager = new TweenManager();
		Timeline.createSequence()
	/*		.push(Tween.set(waveSpr.get(1), SpriteAccessor.POS_XY).target(waveSpr.get(1).getX(), waveSpr.get(1).getY()))
			.push(Tween.set(waveSpr.get(3), SpriteAccessor.POS_XY).target(waveSpr.get(3).getX(), waveSpr.get(3).getY()))
			.push(Tween.set(waveSpr.get(5), SpriteAccessor.POS_XY).target(waveSpr.get(5).getX(), waveSpr.get(5).getY()))
			.push(Tween.set(waveSpr.get(7), SpriteAccessor.POS_XY).target(waveSpr.get(7).getX(), waveSpr.get(7).getY()))
			.push(Tween.set(waveSpr.get(9), SpriteAccessor.POS_XY).target(waveSpr.get(9).getX(), waveSpr.get(9).getY()))
		*/    .beginParallel() 
				.push(Tween.to(waveSpr.get(1), SpriteAccessor.POS_XY, 0.5f).target(waveSpr.get(1).getX()+100, waveSpr.get(1).getY()).ease(Linear.INOUT).delay(0.5f))
				.push(Tween.to(waveSpr.get(3), SpriteAccessor.POS_XY, 0.5f).target(waveSpr.get(3).getX()-100, waveSpr.get(3).getY()).ease(Linear.INOUT).delay(0.5f))
				.push(Tween.to(waveSpr.get(5), SpriteAccessor.POS_XY, 0.5f).target(waveSpr.get(5).getX()+100, waveSpr.get(5).getY()).ease(Linear.INOUT).delay(0.5f))
				.push(Tween.to(waveSpr.get(7), SpriteAccessor.POS_XY, 0.5f).target(waveSpr.get(7).getX()-100, waveSpr.get(7).getY()).ease(Linear.INOUT).delay(0.5f))
				.push(Tween.to(waveSpr.get(9), SpriteAccessor.POS_XY, 0.5f).target(waveSpr.get(9).getX()+100, waveSpr.get(9).getY()).ease(Linear.INOUT).delay(0.5f))
			.end()
			.repeatYoyo(Tween.INFINITY, 0.5f)
			.start(manager);
		
		batch = new SpriteBatch();
		camera = new OrthographicCamera();
		camera.setToOrtho(false, width, height);
		
		controller = new GestureController(lifeSaver);
		gestureDetector = new GestureDetector(20, 0.5f, 2, 0.15f, controller);
		Gdx.input.setInputProcessor(gestureDetector);

		wave = new Wave(0, 300);
		
		font = new BitmapFont();
		font.setScale(font.getScaleX()*2);
		font.setColor(1.0f, 1.0f, 1.0f, 1.0f);
		
		font2 = new BitmapFont();
		font2.setScale(font2.getScaleX()*2);
		font2.setColor(1.0f, 1.0f, 1.0f, 1.0f);
	}

	@Override
	public void dispose() {
		manager.killAll();
		texture.dispose();
		lifeSaverTex.dispose();
		batch.dispose();
	}

	@Override
	public void render() {		
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		controller.update();
		camera.update();

		manager.update(Gdx.graphics.getDeltaTime());

		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		batch.draw(texture, 0, 0, texture.getWidth() * 2, texture.getHeight() * 2);	
		
		Iterator<Sprite> iterator = waveSpr.iterator();

		while(iterator.hasNext()) {
			Sprite spr = iterator.next();
			spr.draw(batch);	
		}

		batch.draw(lifeSaverTex, lifeSaver.x, lifeSaver.y);	
		font.draw(batch, "wave: "+wave.getX()+" - "+wave.getY(), 10, 600);
		font2.draw(batch, "->"+Gdx.graphics.getDeltaTime(), 10, 550);
		batch.end();
	}

	@Override
	public void resize(int width, int height) {
	}
	
	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	public boolean needsGL20 () {
		return false;
	}
	
}
